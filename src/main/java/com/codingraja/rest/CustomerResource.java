package com.codingraja.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codingraja.domain.Customer;
import com.codingraja.service.CustomerService;

@RestController
@RequestMapping("/api")
public class CustomerResource {
	
	private static final String ENTITY_NAME = "customer";
	
	@Autowired
	private final CustomerService customerService;
	
	public CustomerResource(CustomerService customerService) {
		this.customerService = customerService;
	}

	@PostMapping("/customers")
	public ResponseEntity<Customer> createCustomer(@Valid @RequestBody Customer customer) throws Exception {
		if (customer.getId() != null) {
            throw new Exception("A new Customer cannot already have an ID");
        }
		
        Customer result = customerService.save(customer);
        return ResponseEntity.created(new URI("/api/customers/"))
            .header(ENTITY_NAME, result.getId().toString())
            .body(result);
	}
	
	@PutMapping("/customers")
	public ResponseEntity<Customer> createUpdate(@Valid @RequestBody Customer customer) throws Exception {
		if (customer.getId() == null) {
            return createCustomer(customer);
        }
		
        Customer result = customerService.save(customer);
        return ResponseEntity.ok()
            .header(ENTITY_NAME, result.getId().toString())
            .body(result);
	}
	
	@GetMapping("/customers/{id}")
	public ResponseEntity<Customer> getCustomer(@PathVariable Long id) throws URISyntaxException {
		Customer result = customerService.findOne(id);
		
		if(result == null)
	        return ResponseEntity.ok()
	            .header(ENTITY_NAME,"ID does not exits")
	            .body(result);
		else
			return ResponseEntity.ok()
		            .header(ENTITY_NAME, result.getId().toString())
		            .body(result);
	}
	
	@GetMapping("/customers")
	public List<Customer> getAllCustomers() {
		return customerService.findAll();
	}
	
	@DeleteMapping("/customers/{id}")
	public ResponseEntity<Void> deleteCustomer(@PathVariable Long id) throws URISyntaxException {
		customerService.delete(id);;
        return ResponseEntity.ok()
            .header(ENTITY_NAME, id.toString())
            .build();
	}

}
